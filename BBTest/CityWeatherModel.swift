//
//  CityWeatherModel.swift
//  BBTest
//
//  Created by Хазиахметов Ильнур on 23.10.16.
//  Copyright © 2016 Хазиахметов Ильнур. All rights reserved.
//

import UIKit

class CityWeatherModel: NSObject {
    
    var id: Int
    var name: String
    var temp: Int
    var windSpeed: Int
    var windDirection: Int
    
    init(id: Int, name: String, temp: Int, windSpeed: Int, windDirection: Int) {
        self.id = id
        self.name = name
        self.temp = temp
        self.windSpeed = windSpeed
        self.windDirection = windDirection
    }

}
