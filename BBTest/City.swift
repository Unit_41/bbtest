//
//  City.swift
//  BBTest
//
//  Created by Хазиахметов Ильнур on 23.10.16.
//  Copyright © 2016 Хазиахметов Ильнур. All rights reserved.
//

import Foundation
import RealmSwift

class City: Object {
    
    dynamic var id = 0
    dynamic var name = ""
    dynamic var temp = 0
    dynamic var windSpeed = 0
    dynamic var windDirection = 0
    
// Specify properties to ignore (Realm won't persist these)
    
//  override static func ignoredProperties() -> [String] {
//    return []
//  }
}
