//
//  FindCityViewController.swift
//  BBTest
//
//  Created by Хазиахметов Ильнур on 23.10.16.
//  Copyright © 2016 Хазиахметов Ильнур. All rights reserved.
//

import UIKit
import Alamofire
//import Realm
import RealmSwift

class FindCityViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var cityNameTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    var searchResponse = NSMutableArray()
    var selectedCitiesArray = NSMutableArray()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        self.hideKeyboardWhenTappedAround()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cityFoundCell", for: indexPath) as! FoundCityCell
        
//        if searchResponse.count > 0 {
//            if let dict = searchResponse[indexPath.row] as? NSDictionary{
//                cell.nameLabel.text = dict.value(forKey: "name") as! String?
//            }
//        }
        if searchResponse.count > 0 {
            if let city = searchResponse[indexPath.row] as? CityWeatherModel{
                cell.nameLabel.text = city.name
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResponse.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! FoundCityCell
        
        if cell.selectedCell == false {
            cell.selectedCell = true
            cell.contentView.backgroundColor = UIColor.green
            selectedCitiesArray.add(indexPath.row)
        }
        else {
            cell.selectedCell = false
            cell.contentView.backgroundColor = UIColor.white
            selectedCitiesArray.removeObject(identicalTo: indexPath.row)
        }
        
        
    }

    @IBAction func findButtonTapped(_ sender: AnyObject) {
        loadingIndicator.startAnimating()
//        http://api.openweathermap.org/data/2.5/find?q=London&type=like&mode=json&appid=7cdc4a6a2c1b3f14c904eebfc2cd0cc6
        
//        cityNameTextField.text = "l"

        
        if (cityNameTextField.text?.isEmpty)! {
            loadingIndicator.stopAnimating()
        }
        else {
            let urlString = "http://api.openweathermap.org/data/2.5/find?q=\(self.cityNameTextField!.text!)&type=like&mode=json&appid=7cdc4a6a2c1b3f14c904eebfc2cd0cc6"
//            let urlString = "http://api.openweathermap.org/data/2.5/find?q=London&type=like&mode=json&appid=7cdc4a6a2c1b3f14c904eebfc2cd0cc6"
            Alamofire.request(urlString).validate().responseJSON { response in
                switch response.result {
                case .success:
                    print(response)
                    if let result = response.result.value {
                        let searchResponsed = (result as! NSDictionary).value(forKey: "list") as! NSArray
                    
                        for index in 0...searchResponsed.count-1 {
                            
                            let dict = searchResponsed[index] as! NSDictionary
                            let mainDict = dict["main"] as! NSDictionary
                            let windDict = dict["wind"] as! NSDictionary
                            
                            let cityModel = CityWeatherModel(id: dict.value(forKey: "id") as! Int,
                                                             name: dict.value(forKey: "name") as! String,
                                                             temp: mainDict["temp"]! as! Int,
                                                             windSpeed: windDict["deg"] as! Int,
                                                             windDirection: windDict["speed"] as! Int )
                            
                            self.searchResponse[index] = cityModel
                        }
                    }
                    self.tableView.reloadData()
                    print(self.searchResponse)
                    print("JSON Success")
                    self.loadingIndicator.stopAnimating()
                case .failure(let error):
                    print(error)
                    self.loadingIndicator.stopAnimating()
                }
            }
        }
}
        
    @IBAction func addCityButtonTapped(_ sender: AnyObject) {
    
        if searchResponse.count > 0 {
            let realm = try! Realm()
            for index in 0...self.selectedCitiesArray.count-1 {
                let city = City()
                let selectedCity = searchResponse[selectedCitiesArray[index] as! Int] as! CityWeatherModel
                
                city.id = selectedCity.id
                city.name = selectedCity.name
                city.temp = selectedCity.temp
                city.windDirection = selectedCity.windDirection
                city.windSpeed = selectedCity.windSpeed
                
    //            city = searchResponse[selectedCitiesArray[index] as! Int] as! City
                
                try! realm.write {
                    realm.add(city)
                }
            }
            
            self.navigationController?.popViewController(animated: true)
        }
        else {}
    }
    
    @IBAction func backButtonTapped(_ sender: AnyObject) {
        
        self.navigationController?.popViewController(animated: true)
    }

}
