//
//  UserCityCell.swift
//  BBTest
//
//  Created by Хазиахметов Ильнур on 23.10.16.
//  Copyright © 2016 Хазиахметов Ильнур. All rights reserved.
//

import UIKit

class UserCityCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var windDirectionLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
