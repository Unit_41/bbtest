//
//  FoundCityCell.swift
//  BBTest
//
//  Created by Хазиахметов Ильнур on 23.10.16.
//  Copyright © 2016 Хазиахметов Ильнур. All rights reserved.
//

import UIKit

class FoundCityCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    var selectedCell = Bool()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
